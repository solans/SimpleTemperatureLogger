#!/usr/bin/env python
##################################################
#
# Monitoring of resistivity measurements with Arduino
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Sascha.Dungs@cern.ch
# July 2016
#
##################################################

import os
import sys
import math
import ROOT
import array
import time
import argparse
import SerialCom
import realtime
import signal

parser = argparse.ArgumentParser()
parser.add_argument('-o','--outdir',help='output directory',default='/tmp')
parser.add_argument('-f','--fast',action='store_true',help='use fast drawing')
parser.add_argument('-P','--port',help='usb port name',default='/dev/cu.usbmodem411')
parser.add_argument('-c','--channel',type = int, help='number of channels', default="1")
args = parser.parse_args()

cont = True
def signal_handler(signal, frame):
    print('You pressed ctrl+C')
    global cont
    cont = False
    return

signal.signal(signal.SIGINT, signal_handler)


timestamp = array.array('L',(0,))    #seconds since epoch

ROOT.gStyle.SetPadLeftMargin(0.14)
ROOT.gStyle.SetPadBottomMargin(0.15)
ROOT.gStyle.SetPadRightMargin(0.15)
ROOT.gStyle.SetPadTopMargin(0.01)
ROOT.gStyle.SetLabelSize(0.07,"x")
ROOT.gStyle.SetLabelSize(0.07,"y")
ROOT.gStyle.SetTitleSize(0.07,"x")
ROOT.gStyle.SetTitleSize(0.07,"y")
ROOT.gStyle.SetTitleOffset(1.10,"y")
ROOT.gStyle.SetOptTitle(0);

c1 = ROOT.TCanvas("c1","Temperature monitoring", 800,600)
gT = realtime.Plot("T",";Time [s]; Temperature [C]")
for i in range(args.channel): gT.AddPlot("PL")

ser = SerialCom.SerialCom(args.port, baudrate=9600, terminator="\r\n")
time.sleep(0.3)

fw = open("%s/Temperature_%s.txt" % (args.outdir,time.strftime('%Y-%M-%d-%H-%m-%s')),"w")

startTime = 0

while cont:
    line = ser.read()
    print(line)
    line=line.strip()
    if len(line)==0: continue
    k = line[0:1]
    v = line[1:].strip()
    print "Parse: k=%s,v=%s  %s" % (k,v,line)
    if k=="t":
        #v=int(v)+startTime #not used
        timestamp[0]= int(time.time())
        if startTime==0: startTime=timestamp[0]
        c1.Modified()
        ROOT.gSystem.ProcessEvents()
        fw.write("t: %s\n"%time.strftime("%H:%M:%S", time.gmtime(timestamp[0])))
        pass
    elif k=="T":
        net=v[0]
        rst=v[3:]
        chn=int(net)
        if (chn > args.channel-1) : continue
        temp= float(rst)
        #print " arg=%i net=%s, chn=%i, temp=%.2f" % (args.channel,net,chn,temp)
        gT.AddPoint(chn,timestamp[0]-startTime,temp)
        gT.Draw("A")
        if not args.fast: c1.Update()
        fw.write("T : %s %.1f\n"%(chn,temp))
        pass
    pass

fw.close()
ser.close()
