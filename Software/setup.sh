#!/bin/bash
echo "Setup ROOT"
source /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.24/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh 
echo "Setup Python"
export PATH=/afs/cern.ch/sw/lcg/external/Python/2.7.4/x86_64-slc6-gcc48-opt/bin:$PATH 
export LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/external/Python/2.7.4/x86_64-slc6-gcc48-opt/lib:$LD_LIBRARY_PATH 
export PYTHONPATH=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.24/x86_64-slc6-gcc48-opt/root/lib:$PYTHONPATH 
