#!/usr/bin/env python
#############################################
# SerialCom 
# Base serial communication class
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

import io
import serial
import time

class SerialCom:
    com = None
    sio = None
    trm = None
    def __init__(self, portname,baudrate=9600,terminator="\r\n",timeout=0.5):
        self.com = serial.Serial(port=portname,baudrate=baudrate,timeout=timeout)
        self.sio = io.TextIOWrapper(io.BufferedRWPair(self.com,self.com))
        self.trm = terminator
        pass
    def open(self):
        self.com.open()
        pass
    def close(self):
        self.com.close()
        pass
    def write(self,cmd):
        cmd=cmd.replace("\r","").replace("\n","")+self.trm
        #print "SerialCom:write %s" %cmd
        self.sio.write(unicode(cmd))
        self.sio.flush()
        pass
    def writeAndRead(self,cmd):
        self.write(cmd)
        #time.sleep(3)
        return self.read()
    def read(self):
        try:
            #return self.sio.readline().encode("utf8").replace("\r","").replace("\n","")
            s=self.sio.readline()
            #print "SerialCom:read %s" % s
            s=s.encode("utf8").replace("\r","").replace("\n","")
            return s
        except:
            return ""

    
