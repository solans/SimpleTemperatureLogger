/*
 NtcReadout with uno
*/

#include <math.h>

const int temperatureCH1_pin = A1;
const int temperatureCH2_pin = A0;
float temperatureCH1;
float temperatureCH2;

int incomingByte = 0;
String commands = "";
int m_debug=0;
int m_chdelay=50;
int m_evdelay=1000;
float m_tendloop=-1000;

void setup(){
 Serial.begin(9600);
 //wait for serial
 while (!Serial){;}
 //set NTC analog reference
 analogReference(DEFAULT);
 }

void loop(){
  // read any data
  if (Serial.available())
  {
    while(Serial.available())
    {
      commands += (char)Serial.read();
    }
  }
  //Process requests ... 
  //don't run read-out until event time delay has passed
  if ((millis()-m_tendloop)<m_evdelay) return;

  //Read-out all the channels
  String out = "t: " + String(millis());
  Serial.println(out);
  temperatureCH1 = get_temperature(temperatureCH1_pin);
  temperatureCH2 = get_temperature(temperatureCH2_pin);
  out = "T0: " + String(temperatureCH1);
  Serial.println(out);
  out = "T1: " + String(temperatureCH2);
  Serial.println(out); 
  m_tendloop = millis();
}

float get_temperature(int pin)
{
 analogRead(pin); 
 float adc = analogRead(pin);
 float voltage = adc * 5.0 / 1023.0;
 float resistance = 100000*(5.0/voltage - 1);
 if (resistance <= 0)
 {
  resistance = 0.00000001;
 }
 return 1./((1./4120)*log(resistance/100000)+(1./298.15))-273.15;
}
