###########################################################

Simple Temperature Logger

###########################################################

0. Description

 Arduino based temperature logger that outputs 
 the temperature of an NTC to a ROOT plot and a log file.

1. Folder contents

 Software: read-out application for Linux/Mac and required modules.
 FwSerialT: firmware for arduino board.

2. How to run the code

2.1. If you have ROOT and python bindings for ROOT on a Linux/Mac

 Change to the Software directory and run the simple_T_monitoring application

 python simple_T_monitoring_arduino.py [-o OUTDIR] [-P PORT]

 where:
 -o OUTDIR  specifies the output directory
 -P PORT    usb port name (/dev/ttyUSB1, /dev/cu.usbmodem1411)

2.2. If you have access to AFS on a Linux/Mac

 Change to the Software directory and source the setup.sh file. 
 Then follow 2.1.

3. Credits

 Sascha.Dungs@cern.ch
 Abhishek.Sharma@cern.ch
 Carlos.Solans@cern.ch
 
 ATLAS ADE Pixel Group
 http://ade-pixel-group.web.cern.ch

